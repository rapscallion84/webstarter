# Webstarter readme

The purpose of this project is to provide a barebones template for building a website from scratch. The instructions provided within this project will walk you through the process of building a website using the following technologies and tools:

**Languages:**

* HTML
* CSS / SCSS
* JavaScript
* Markdown

**Tools:**

* Node / NPM
* Gulp
* Nunjucks

## Preparation

1. Install an editor - I strongly recommend Visual Studio Code: https://code.visualstudio.com
2. Install Git - https://git-scm.com/downloads (on Macs, install XCode)
3. You will need easy access to the bash terminal
    * Open Visual Studio Code
    * Open the command palette using `ctrl + shift + p`
    * Type or select `Open User Settings` from the command palette
    * Click the `{ }` icon in the top right
    * In the right hand side panel, within the existing `{ }` brackets, paste the following: `"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe"` and save
    * You should now be able to open the terminal from View > Terminal
4. Install Node: https://nodejs.org/en/download/

## Next steps

Follow the steps outlined in the embedded markdown files in sequence:

1. [Initialisation](/docs/1-initialisation/1-initialisation.md)
2. [Getting started](docs/2-getting-started/2-getting-started.md)