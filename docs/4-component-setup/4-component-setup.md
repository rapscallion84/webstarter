# Component setup

A website contains of multiple pages with each page consisting of multiple components. There are many ways to structure your code to make development easier, but for the purposes of this tutorial we will be storing all of our component styles in the `src/assets/scss` folder.

## Understanding component based design

Broadly speaking, your `scss` stylesheets will be either a `core` stylesheet or a `component` stylesheet. Your `main.scss` file will be a single stylesheet containg a list of imports that combine all of the core and component stylesheets.

* `core` stylesheets typically contain global variables and styles such as colours and fonts
* each `component` stylesheet with contain all of the styles for one particular component, e.g. a header or navbar

### Create the filesystem

To keep our code tidy, create three folders inside the `src/assets/scss` folder:

* `core`
* `components`
* `helpers`

Inside the `core` folder, create three new `.scss` files named:

* `_colours.scss`
* `layout.scss`
* `_typography.scss`

The `_` character at the beginning of the filenames is optional, but is often used as a convention when that file represents a partial stylesheet - i.e. that it will be imported into a parent file for use. When referencing files in the filesystem, `scss` automatically recognises the `_` character so you do not need to type it for the imports.

Inside the `components` folder, create three new files named:

* `cards.scss`
* `hero-banner.scss`
* `navbar.scss`

Inside the `helpers` folder, create a `_mixins.scss` file.

### Add the imports to main.scss

Now, open up your existing `main.scss` and replace all content with the following:

```scss
// Helpers
@import 'helpers/mixins';

// Core
@import 'core/colours.scss';
@import 'core/layout.scss';
@import 'core/typography.scss';

// Components
@import 'components/cards.scss';
@import 'components/hero-banner.scss';
@import 'components/navbar.scss';
```

It's important to list the `core` imports above the `component` imports, because components will reference the core styles. Likewise, `helpers` are listed above `core` because core styles may require use of mixins.

## Creating the core styles

Now we will create our core styles and some simple components. First, let's create our properties and mixin that will help with creating responsive designs.

### Set up breakpoints

Open `core/_layout.scss` and add the following:

```scss
// Breakpoints
$breakpoints: ('tablet': (min-width: 767px),
  'laptop': (min-width: 992px),
  'desktop': (min-width: 1200px)) !default;

// Layout
body {
  margin: 0;
}

section > div {
  width: 90%;
  margin: 0 auto;
}
```

The `$breakpoints` property contains the `min-width` specifications for various device sizes. This will be important for creating responsive components across different device sizes.

### Create the breakpoint mixin

Open `core/helpers/_mixins.scss` and paste the following:

```scss
// Device breakpoints
@mixin breakpoint($breakpoint) {
  @if map-has-key($breakpoints, $breakpoint) {
    @media #{inspect(map-get($breakpoints, $breakpoint))} {
      @content;
    }
  }

  @else {
    @warn "No value could be retrieved from `#{$breakpoint}`. "
    +"Available breakpoints are: #{map-keys($breakpoints)}.";
  }
}
```

This mixin will use the `$breakpoint`property created in the previous step to determine the device widths. During compilation from `scss` to `css`, this mixin will result in a set of media queries that will be used by our components to create responsive designs.

### Set up global colours

Now we will set some colours to be used globally across the website. Open the `core/_colours.scss` file and paste the following:

```scss
$primary: #f34a4a;
$secondary: #464646;
$tertiary: #fff;

$shadow: #64646f33;

$success: #9cd881;
$warn: #fdd66a;
$danger: #fd7777;
```

### Set up global typography

Next, open `core/_typography.scss` and paste the following:

```scss
h1,
h2,
h3,
h4 {
  margin: 0;
  text-transform: uppercase;
  letter-spacing: 3px;
}

a {
  text-decoration: none;
  color: $tertiary;
}

h1 {
  font-size: 30px;

  @include breakpoint('tablet') {
    font-size: 50px;
  }

  @include breakpoint('laptop') {
    font-size: 50px;
  }
}

h2 {
  font-size: 25px;

  @include breakpoint('tablet') {
    font-size: 35px;
  }

  @include breakpoint('laptop') {
    font-size: 45px;
  }
}

h3 {
  font-size: 20px;

  @include breakpoint('tablet') {
    font-size: 30px;
  }

  @include breakpoint('laptop') {
    font-size: 40px;
  }
}

h4,
p {
  font-size: 16px;

  @include breakpoint('tablet') {
    font-size: 18px;
  }
}
```

If browsersync is still running (or if you quit and restart `gulp`), your webpage should now look like this:

![4-1.png](4-1.png 'New typography')

## Create the component styles

Now that the core styles are established, we can focus on building some basic components.

### Navbar

We'll start by building a simple navigation bar. Open `components/_navbar.scss` and paste the following:

```scss
// Navbar
.navbar {
  background: $primary;

  ul {
    margin: 0 auto;
    padding: 0;
    list-style: none;
  }

  ul li {
    display: block;
    line-height: 21px;

    @include breakpoint('tablet') {
      display: inline-block;
    }
  }

  ul li a {
    display: block;
    text-decoration: none;
    color: $tertiary;
    padding: 8px 25px;
  }

  ul li a:hover {
    color: $tertiary;
    background: $secondary;
  }

  ul li ul.dropdown {
    display: none;
    width: 200px;
    position: absolute;
    z-index: 2;
  }

  ul li:hover ul.dropdown {
    display: block;
    background: $secondary;
    left: 25px;

    @include breakpoint('tablet') {
      left: auto;
    }
  }

  ul li ul.dropdown li {
    display: block;

    a:hover {
      background: $primary;
      color: $tertiary;
    }
  }
}
```

Now let's add some markup that utlises our new stylesheet. In the `index.html` file, remove the contents in the `<body></body>` tags and paste the following:

```html
<!-- Navbar -->
<nav class="navbar">
  <ul>
    <li><a href="/index.html">Home</a></li>
    <li><a href="/news.html">News</a></li>
    <li>
      <a href="#" onclick="void(0)">Recipes &#9662;</a>
      <ul class="dropdown">
        <li><a href="/recipes/starters.html">Starters</a></li>
        <li><a href="/recipes/mains.html">Mains</a></li>
        <li><a href="/recipes/desserts.html">Desserts</a></li>
        <li><a href="/recipes/bread.html">Bread</a></li>
      </ul>
    </li>
    <li><a href="/history.html">History</a></li>
    <li><a href="/about.html">About us</a></li>
  </ul>
</nav>
```

Upon refresh, your page should now look like this on mobile:

![4-2.png](4-2.png 'Navbar - mobile')

and like this on tablet and deskop:

![4-3.png](4-3.png 'Navbar - tablet and desktop')

### Hero banner

The hero banner - sometimes referred to as a jumbotron - is large marquee banner at the top of the page, immediately following the navbar.

In the `_hero-banner.scss` file, paste the following:

```scss
.hero-banner {
  display: flex;
  flex-direction: column;
  height: 100px;
  background: $secondary;
  color: $tertiary;
  padding: 20px;

  @include breakpoint('tablet') {
    height: 200px;
  }
}

.hero-banner__title {
  border-bottom: 1px solid;
}

.hero-banner__subtitle {
  font-style: italic;
}

.hero-banner__cta {
  background: $success;
  border: none;
  padding: 5px 20px;
  border-radius: 5%;
  cursor: pointer;
  transition: 0.5s;
  width: 100%;
  margin-top: auto;

  &:hover {
    background: $primary;
    color: $tertiary;
    transition: 0.5s;
  }

  @include breakpoint('tablet') {
    max-width: 150px;
  }
}
```

And in the `index.html` file, beneath your previous navbar code but still within the `<body></body>` tags, paste the following:

```html
<!-- Hero banner -->
<div class="hero-banner">
  <h1 class="hero-banner__title">DLG chefs' club</h1>
  <p class="hero-banner__subtitle">A place where DLG staff can share their best recipes</p>
  <button class="hero-banner__cta">Get started</button>
</div>
```

Upon refresh, your page should now look like this on mobile:

![4-4.png](4-4.png 'Hero banner - mobile')

and like this on tablet and deskop:

![4-5.png](4-5.png 'Hero banner - tablet and desktop')

### Cards

For our last component, we wish to display multiple grouped items in an aesthetic way.

In `_cards.scss` paste the following:

```scss
.cards {
  display: flex;
  flex-direction: column;
  
  @include breakpoint('tablet') {
    padding: 40px 0;
    flex-direction: row;
    justify-content: space-around;
  }
}

.card {
  display: flex;
  flex-direction: column;
  padding: 20px;
  box-shadow: 0px 7px 29px 0px rgba(100, 100, 111, 0.2);
  transition: 0.3s;

  &:hover {
    transition: 0.3s;
    background: $primary;
    transform: scale(1.05)
  }

  @include breakpoint('tablet') {
    width: 20%;

    &:hover {
      transform: scale(1.1);
    }
  }
}

.card__title {
  text-transform: none;
  margin-bottom: 10px;
  color: $secondary;
  text-align: center;
}

.card__image {
  width: 100%;
  height: 200px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: auto 100%, cover;
}

.card__text {
  color: $secondary;
  text-align: center;
}

.card__image--starters {
  background-image: url('https://realfood.tesco.com/media/images/1400x919-CHRISTMAS-SALAD-WINTER-WREATH-TRF-d8c3c0d1-316c-4e0e-a936-26dc81f429df-0-1400x919.jpg');
}

.card__image--mains {
  background-image: url('http://www.simplybeefandlamb.co.uk/media/1619/lamb-roast-dill-and-lemon-web.jpg?width=1600&height=900&mode=crop');
}

.card__image--desserts {
  background-image: url('https://realfood.tesco.com/media/images/RFO-MAIN-472x310-TropicalTrifle-5a2067b7-2491-42be-8353-a761924fe855-0-472x310.jpg');
}

.card__image--bread {
  background-image: url('https://static.onecms.io/wp-content/uploads/sites/9/2017/07/free-bread-baskets-ft-blog0717.jpg');

}
```

Once again, in within the `<body></body>` tags of the `index.html` file, place the following code beneath your `hero-banner`:

```html
<!-- Cards -->
<div class="cards">
  <div class="card">
    <h2 class="card__title">Starters</h2>
    <div class="card__image card__image--starters"></div>
    <p class="card__text">A little something to get things started</p>
  </div>
  <div class="card">
    <h2 class="card__title">Mains</h2>
    <div class="card__image card__image--mains"></div>
    <p class="card__text">This is what you came for</p>
  </div>
  <div class="card">
    <h2 class="card__title">Desserts</h2>
    <div class="card__image card__image--desserts"></div>
    <p class="card__text">The perfect way to end the evening</p>
  </div>
  <div class="card">
    <h2 class="card__title">Bread</h2>
    <div class="card__image card__image--bread"></div>
    <p class="card__text">No meal is complete without it</p>
  </div>
</div>
```

All being well, upon refresh, your page should now look like this on mobile:

![4-6.png](4-6.png 'Cards - mobile')

and like this on tablet and deskop:

![4-7.png](4-7.png 'Cards - tablet and desktop')

[Back to: 3. Browsersync](../3-browsersync/3-browsersync.md)