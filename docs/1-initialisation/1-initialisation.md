# Initialisation

These steps will help you create your project and set it up for version control.

## Git

First, start by cloning down this repository.

1. Open VS Code and open the Explorer (View > Explorer) if it's not already open
2. Click the `Open Folder` button and select a directory on your local disk where you wish to put your project
3. Open the terminal and then paste the following command: `git clone https://github.com/rapscallion84/webstarter.git`
   * This will copy this repository's master branch to your local disk
   * At this point, the only file that will be copied to your disk is this readme file
4. Create a new 'develop' branch for all your work using `git checkout -b develop`
5. Committing and pushing your work will be covered later

## Node

You should now have your project open in VS Code. Ensure that you always have the terminal open and that you are in the 'develop' branch (use the terminal command `git branch` to check). When you open the terminal in VS Code with a project open, it will always default the path to the project's root directory.

1. Initialise NPM in your project directory by using `npm init --yes` command in the terminal
    * The optional `--yes` flag skips the setup questions
2. This will create a `package.json` file in your project

## Project structure

It's important to create and maintain a logical folder structure for your project. You will need a single `src` folder that will contain all your source code, with a set of child folders as follows (the trailing `/` denotes a folder and `|--` denotes items within that folder):

```bash
docs/
src/
|-- assets/
  |-- img/
  |-- js/
  |-- scss/
|-- pages/
package.json
README.md
```

* `src/` is a folder that will contain all your source code
  * `assets/` is a folder that will contain your images (`img/`), scripts (`js/`) and style (`scss/`) files.
* `pages/` is a folder that will contain all your markup (HTML) files
* `package.json` is a file that sits at the root of the project directory, which you created in the previous step
* `README.md` is a markdown file in the root of the project directory - it's this file!

## Create a .gitignore file

Version control through git is a powerful tool; it permits parallel development, the capability to reverse edits and so much more. When pushing commits to a remote repository, you might not want to copy some files or folders due to their size or sensitiviity. One such folder is `node_modules`, which is automatiically created for you when you install your first package using `npm`.

The `node_modules` folder will contain weighty third-party code that your site or application needs to function, but it will take up size on disk or in your repository, making pushing commits (uploading files) slower. The `package.json` file provides all the information necessary for another user to get any necessary packages themselves through the `npm install` command, so these packges do not need to exist in your repository.

We can exclude files or folders from git through use of a `.gitignore` file created at the root of your project. Create a file called `.gitignore` - including the `.` at the front and omitting any file extension. Open it in your editor and write `node_modules` before saving and closing the file. Your project should now look like this:

```text
docs/
src/
  |-- assets/
    |-- img/
    |-- js/
    |-- scss/
  |-- pages/
.gitignore
package.json
README.md
```

To exclude more items from git, type their names on new lines such as:

```text
node_modules
file1
folder1
file2
```



[Back to: Readme](/README.md)

[Proceed to: 2. Getting started](../2-getting-started/2-getting-started.md)