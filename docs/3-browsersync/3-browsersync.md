# Live server with browser-sync

During development, it will be useful to have a live preview of your website running. There are many ways to acheive this, but we will be using a package called `browsersync` to do so.

## Install browser-sync

In the terminal, run the `npm i browser-sync --save-dev` command.

## Configure the gulpfile

Once the package is installed, we need to make some ammendments to the `gulpfile.js` to utilise it.

### Import browser-sync

First we need to import `browser-sync` like another other package. This line can go below all your other imports:

```js
var browser = require('browser-sync');
```

### Create the watch task

Browser-sync will be able to monitor your filesystem for changes in the code and then refresh the browser automatically. To do this, we must create a task. Place this code above your `build` task:

```js
// Watch:
gulp.task('watch', function () {
  // Start browser-sync
  browser.init({
    injectChanges: true,
    open: true,
    browser: 'google chrome',
    server: {
      baseDir: './dist/'
    }
  });

  // Watch folders
  gulp.watch('src/lib/scss/**/*.scss', gulp.series('styles'));
  gulp.watch('src/pages/**/*.html', gulp.series('pages'));
  gulp.watch('gulpfile.js', gulp.series('build'));
});
```

* The first part of this task initialises browser-sync with some configuration settings
  * `injectChanges: true` tells browsersync to inject any stylesheet changes without performing a full page refresh
  * `open: true` specifies that when the task is executed, browser-sync will launch the browser automatically
  * `browser: 'google chrome'` specifies the browser to launch
  * `baseDir: './dist/'` tells browser-sync which folder is the root of your website
* The second part sets up a collection of folders on your local disk for browser-sync to observe during development. The first paramter of `watch()` is the path (including filetype) and the second parameter specifies the task that will execute once a change is observed in the path specified by the first parameter
  * Any change to an `scss` file in `src/lib/scss` results in the `styles` task executing
  * Any change to an `html` task in `src/pages` results in the `pages` task executing
  * Any change to the `gulpfile.js` file in particular results in the entire `build` task executing

### Integrate watch task as part of your default task

Let's ensure that browser-sync launches automatically when `gulp` (the default task) is run. Find the following code:

```js
// Default: Run the build task
gulp.task('default',
  gulp.series('build')
);
```

and change it to:

```js
// Default: Run the build task then start watch
gulp.task('default',
  gulp.series('build', 'watch')
);
```

### Update subtasks

In the `watch` task we set up 3 folders to be observed, each having a different task that will be triggered once a change is detected in its respective folder. Now we need to add an extra line of code to each of those tasks that will inform browser-sync to refresh the browser once the task is completed. This can be done by adding the following line of code at the end of the pipe:

```js
.pipe(browser.stream());
```

1. Find the `pages` task and change it to the following:

```js
// Pages: Copy all HTML pages into dist
gulp.task('pages', function () {
  return gulp.src('src/pages/**/*')
    .pipe(gulp.dest('dist'))
    .pipe(browser.stream());
});
```

2. Find the `styles` task and change it to the following:

```js
// Styles: Compile SASS into CSS in dist/assets/css
gulp.task('styles', function () {
  return gulp.src('src/assets/scss/**/*')
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(concat('main.css'))
    .pipe(replace('/src', ''))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browser.stream());
});
```

## Run browser-sync

### Run gulp

In the terminal, run `gulp`. Once completed, a new Chrome tab should open to display your website. You should notice that the terminal output from `gulp` is different:

```bash
[13:19:06] Using gulpfile ~/Projects/webstarter/gulpfile.js
[13:19:06] Starting 'default'...
[13:19:06] Starting 'build'...
[13:19:06] Starting 'clean'...
[13:19:06] Finished 'clean' after 18 ms
[13:19:06] Starting 'styles'...
[13:19:06] Starting 'pages'...
[13:19:06] Finished 'pages' after 76 ms
[13:19:06] Finished 'styles' after 78 ms
[13:19:06] Finished 'build' after 97 ms
[13:19:06] Starting 'watch'...
[Browsersync] Access URLs:
 -------------------------------------
       Local: http://localhost:3000
    External: http://192.168.0.26:3000
 -------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 -------------------------------------
[Browsersync] Serving files from: ./dist/
```

This terminal will now be actively engaged by browser-sync.

---

**Note:** If you need to run more terminal commands, e.g. for `git` or `npm` whilst `browser-sync` is running then open a new terminal tab.

---

You can control/command click the `Local:` url (in this case `http://localhost:3000`) to launch another tab linked to browser-sync, in case you close your tab accidentally.

### Make a change to index.html

Have your browser visible next to your code. Open `src/pages/index.html` and beneath the `<h1>Hello world</h1>` add the following:

```html
<p>Live update!</p>
```

Save your changed to `index.html`. You may wish to configure VSCode to autosave a file on focus change, or get used to control/command + S as the save shortcut.

If all is working well, you should see the following appear in the terminal:

```bash
[13:28:22] Starting 'pages'...
[Browsersync] 1 file changed (index.html)
[13:28:22] Finished 'pages' after 12 ms
[Browsersync] Reloading Browsers...
```

Your webpage should refresh automatically and now display the following:

![3-1.png](3-1.png 'Updated webpage')

### Make a change to main.scss

We will now perform the same test for our styles. Open `src/assets/scss/main.scss` and change the code to:

```scss
h1 {
  color: blue;
}
```

If all is working well, you should see the following appear in the terminal:

```bash
[13:38:45] Starting 'styles'...
[Browsersync] 1 file changed (main.css)
[13:38:46] Finished 'styles' after 16 ms
```

Your webpage should refresh automatically and now display the following:

![3-2.png](3-2.png 'Updated webpage 2')

[Back to: 2. Getting started](../2-getting-started/2-getting-started.md)

[Next to: 4. Component setup](../4-component-setup/4-component-setup.md)
