# Getting started

## Create the index.html

We will now create the entrypoint into your website. In the `src/pages/` folder, create a new file called `index.html` and using an editor past in the following content:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>My website</title>
</head>
<body>
  <h1>Hello world</h1>
</body>
</html>
```

If you open the newly created index.html in your browser (click and drag to a browser, or double click from Windows Explorer or Finder) you will see something like this:

![Basic webpage](2-1.png "Basic webpage")

* You can change the document title (the next that appears in the tab of the browser) by editing the content within the `<title></title>` tags
* You can change the h1 header (currently "Hello world") by editing the content within the `<h1></h1>` tags

## Install Gulp

Gulp is a task runner. You can create a JavaScript file with a series of tasks that will run in any particular order, with a mix of parallel and serial execution. We will be using Gulp to compile and build our website from the source (`src`) files into a distribution-ready (`dist`) version.

In VS Code with the project open, launch the integrated terminal (Ctrl + J for Windows or Cmd + J for Mac) and type the following command:

```bash
npm install gulp --save-dev
```

* `npm` is shorthand for Node Package Manager
* `install` is a command for `npm` that will install a package to the `node_modules` folder of your project
* `gulp` is the name of the package we are installing
* `--save-dev` is an optional flag that informs the project that this is a development-only package; i.e. that it's not required for the project to run in production and will not need to be installed on production servers

Once installed, open the `package.json` file and observe the following:

```json
"devDependencies": {
  "gulp": "^4.0.2"
}
```

* `devDependencies` contains a list of all packages that are required for a developer to work with the project
* Packages listed here will **not** be installed in production environments
* Inside the `devDependencies` is a list of key-value pairs with the package name as the key on the left and the version number as the value on the right
* The version number anatomy is `x.y.z`, where `x` is the major version, `y` is the minor version and `z` is the patch number (patches are smaller updates than even minor versions)
* The `^` before the version number means 'Compatible with version', meaning the `npm install` command will install the lastest minor/patch versions up to the major version number (up to `4` in this case), i.e. increments in the second and third numbers
* You may see a `~` instead of `^` and this means 'Approximately equivalent to version', meaning `npm install` will install future versions up to the latest **minor** version (`0` in this case), i.e. increments in the third number
* No symbol before the version specifies an exact version match

## Set up gulpfile.js

Create a new file called `gulpfile.js` at the **root** of your project (**not** in the `src` folder) and open it. This is a JavaScript file like any other.

### Import Gulp

First, we must import the `gulp` package so that we can use its functionality:

```js
var gulp = require('gulp');
```

* We define a new variable called `gulp` using JavaScript's keyword `var`
* It's assigned a value that references the `gulp` library

### Create the 'pages' Gulp task

Now we need to create a Gulp task:

```js
// Pages: Copy all HTML pages into dist
gulp.task('pages', function () {
  return gulp.src('src/pages/**/*')
    .pipe(gulp.dest('dist'));
});
```

When executed, this task will copy all files with the `.html` extension from the `src/pages` directory (and any subdirectoy, indicated by `/**/*` to the `dist` folder (it will create the folder if it doesn't exist). You can use a series of `.pipe()` commands to chain together actions - this is covered later.

### Run Gulp

Before we run Gulp, let's first observe the structure of our project:

```text
docs/
node_modules/
src/
  |-- assets/
    |-- img/
    |-- js/
    |-- scss/
  |-- pages/
    |-- index.html
.gitignore
package-lock.json
package.json
README.md
```

Now, in the terminal, run the following command:

```bash
gulp pages
```

You should see that a new folder called `dist` was created in your project root containing `index.html`. This occurred as a result of running the Gulp task you created above. The `dist` folder will be representative of your website files in production.

### Create the 'clean' Gulp task

When developing, it's useful to have the capability to programatically clear your `dist` folder prior to a new build. Let's create another Gulp task called `clean`; though the position doesn't matter, it might logically make sense to place it above the previous `pages` task but below the variable declarations.

First, install a new package called `del`:

```bash
npm install del --save-dev
```

Under your existing `gulp` declaration, import the package with the following line:

```js
var del = require('del');
```

Beneath this declaration and above the `pages` task, paste the following:

```js
// Clean: Delete the dist folde and its contents
gulp.task('clean', function () {
  return del(['dist']);
});
```

The function `del` takes an array of items that you wish to delete. In this case it's a single folder, so the array contains just one item: `dist`.

You can run the following command in the terminal now:

```bash
gulp clean
```

If successful, you should notice that the `dist` folder and all its contents have been deleted from the project.

## Set up SASS transpiling

SASS (Syntatically awesome style sheets) is a powerful superset of CSS, permitting use of advanced features for development of your website. It uses the `.scss` file extension. The browser is unable to natively interpret SASS, so instead it must be transpiled (converted) into CSS before it's copied to your `dist` folder.

### Install packages

Handily, there is a package we can use that is ready to integrate with our task runner for the purposes of converting our `.scss` files to `.css`. In the terminal, type the following command:

```bash
npm install gulp-sass --save-dev
npm install sass --save-dev
```

When developing our work, we will keep styles as modularised as possible to make development easy. This can lead to having many `.scss` files, but ideally we only want to end up with a single `.css` file to import into our website. We'll use two additional packages here to aid us in combining files and changing their path:

```bash
npm install gulp-concat --save-dev
npm install gulp-replace --save-dev
```

In your `package.json`, your `devDependencies` should now look like this (version numbers may differ):

```json
"devDependencies": {
  "del": "^6.0.0",
  "gulp": "^4.0.2",
  "gulp-concat": "^2.6.1",
  "gulp-replace": "^1.1.3",
  "gulp-sass": "^5.1.0",
  "sass": "^1.49.0"
}
```

### Create the styles Gulp task

In your `gulpfile.js`, add the following declarations beneath the others:

```js
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var sass = require('gulp-sass')(require('sass'));
```

Now let's create a new Gulp task for `styles`:

```js
// Styles: Compile SASS into CSS in dist/assets/css
gulp.task('styles', function () {
  return gulp.src('src/assets/scss/**/*')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(concat('main.css'))
    .pipe(replace('/src', ''))
   .pipe(gulp.dest('dist/assets/css'));
});
```

* `gulp.src()` command references the content of the `src/assets/scss` folder and selects all files contained within this directory and any subdirectories
* `sass()` command transpiles the `scss` to `css`. It has a paramter of `outputStyle` that is set to `compressed`, which means the resultant `css` is minified
* `concat()` causes all files referenced in the first step to be combined into one file called `main.css`
* `replace()` changes the output path from `src/assets/scss` to `src`
* `gulp.dest()` sets the destination of the output to the `dist/assets/css` folder

### Create additional Gulp tasks to handle build

As we add more Gulp tasks, it's going to become more time-consuming to individually run them via the terminal. We will now write two more tasks that will process multiple Gulp tasks.

```js
// Build: Clean the dist folder and then run all build tasks
gulp.task('build',
  gulp.series('clean',
    gulp.parallel('styles', 'pages'))
);
```

This `build` task has a combination of serial and parallel processing. First comes the serial processing - it will run the `clean` task first and wait for completion before moving on to the next task. The next task in this case is in fact a group of tasks that run in parallel. Tasks `styles` and `pages` will be started simultaneously and will end asychronously. Now, running `gulp build` will run all three of your other tasks.

Create one more task:

```js
// Default: Run the build task
gulp.task('default',
  gulp.series('build')
);
```

`default` is a reserved name with associated functionality for Gulp. By naming a task `default`, you make the contents of that task associated with running `gulp` in the console (without any task name specified).

When you run `gulp` now, you should see output in your terminal similar to the following:

```bash
[16:13:29] Using gulpfile ~/Projects/webstarter/gulpfile.js
[16:13:29] Starting 'default'...
[16:13:29] Starting 'build'...
[16:13:29] Starting 'clean'...
[16:13:29] Finished 'clean' after 16 ms
[16:13:29] Starting 'styles'...
[16:13:29] Starting 'pages'...
[16:13:29] Finished 'pages' after 98 ms
[16:13:29] Finished 'styles' after 99 ms
[16:13:29] Finished 'build' after 117 ms
[16:13:29] Finished 'default' after 118 ms
```

## Create an import a custom style

### Create the stylesheet

In the `src/assets/scss` folder, create a new file called `main.scss` and paste the following:

```scss
h1 {
  color: red;
}
```

### Import the stylesheet to index.html

In `src/pages/index.html` paste the following line within the `<head></head>` tags, just under the `<title></title>`:

```html
<link rel="stylesheet" href="assets/css/main.css">
```

### Build your project

In the terminal, run `gulp`. If it is successful, you should see the following file structure appear within your project:

```text
dist/
  |-- assets/
    |-- img/
    |-- js/
    |-- css/
      |-- main.css
  |-- pages/
    |-- index.html
```

Open `index.html` in the browser and you should notice that the font color of the title has changed to red:

![New CSS](2-2.png "New CSS")
## Add dist to .gitignore

Finally, we want to ensure that we don't upload a copy of our build directory to our repository. Open the `.gitingore` file and, on a new line, add `dist`.

[Back to: 1. Initialisation](../1-initialisation/1-initialisation.md)

[Next to: 3. Browsersync](../3-browsersync/3-browsersync.md)
